<?php

namespace App\Http\Controllers;

use App\Hadith;
use Illuminate\Http\Request;
use Validator;
use App\Category;
use App\CategorySubscription;
use App\Tag;
use App\TagSubscription;
use App\Photo;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class HadithsController extends Controller {

    protected $types = ['alqudsiu', 'almarfue', 'almawquf', 'almaqtue', 'alsahih', 'aldaeif'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $hadiths = Hadith::paginate(10);
        return view('admin.hadith.all')->with('hadiths', $hadiths, 'types', $this->types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $types = $this->types;
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.hadith.add')->with('categories', $categories)->with('types', $types)->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title' => 'required|string',
                    'body' => 'required|string',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $hadith = new Hadith();
        $hadith->title = $request->get('title');
        $hadith->body = $request->get('body');
        $hadith->type = $request->get('type');
        if ($hadith->save()) {
            if ($request->hasFile('image')) {
                $images = $request->file('image');
                $photos = [];
                for ($i = 0; $i < count($images); $i++) {
                    $photos[$i]["image"] = $images[$i]->move('uploads/images', $request->title . "_" . rand(100000, 900000) . '.' . $images[$i]->extension());
                    $photos[$i]["hadith_id"] = $hadith->id;
                    $photos[$i]["description"] = $hadith->title;
                    $created_time = Carbon::now();
                    $photos[$i]["updated_at"] = $created_time;
                    $photos[$i]["created_at"] = $created_time;
                }
                Photo::insert($photos);
            }
            if ($request->has('tags')) {
                $tags = $request->get('tags');
                $Newtags = [];
                for ($i = 0; $i < count($tags); $i++) {
                    $Newtags[$i]["hadith_id"] = $hadith->id;
                    $Newtags[$i]["tag_id"] = $tags[$i];
                }
                TagSubscription::insert($Newtags);
            }
            $subscription = new CategorySubscription();
            $subscription->category_id = $request->get('category');
            $subscription->hadith_id = $hadith->id;
            if ($subscription->save()) {
                return redirect()->back()->with('success', 'success');
            }
            return redirect()->back()->with('success', 'success adding hadith but without subscription');
        } else {
            return redirect()->back()->with('fail', "fail");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hadith  $hadith
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $hadith = Hadith::findOrFail($id);
        $images = Photo::where('hadith_id',$id)->get();
        //dd($hadith->images);
        return view('admin.hadith.show')->with('hadith', $hadith)->with('images', $images);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hadith  $hadith
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
              
        $hadith = DB::table('hadiths')
                ->join('categories_subscription', 'categories_subscription.hadith_id', '=', 'hadiths.id')
                ->where('hadiths.id', $id)->get()->first();
        $hadith->tags_ids = TagSubscription::where('hadith_id', $id)->get()->pluck('tag_id')->toArray();        
        //dd($hadith);
        $types = $this->types;
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.hadith.edit')
                        ->with('categories', $categories)
                        ->with('types', $types)
                        ->with('tags', $tags)
                        ->with('hadith', $hadith);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hadith  $hadith
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
                    'title' => 'required|string',
                    'body' => 'required|string',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        }
        $hadith = Hadith::findOrFail($id);
        $hadith->title = $request->get('title');
        $hadith->body = $request->get('body');
        $hadith->type = $request->get('type');
        if ($hadith->save()) {
            if ($request->hasFile('image')) {
                $images = $request->file('image');
                $photos = [];
                for ($i = 0; $i < count($images); $i++) {
                    $photos[$i]["image"] = $images[$i]->move('uploads/images', $request->title . "_" . rand(100000, 900000) . '.' . $images[$i]->extension());
                    $photos[$i]["hadith_id"] = $hadith->id;
                    $photos[$i]["description"] = $hadith->title;
                    $created_time = Carbon::now();
                    $photos[$i]["updated_at"] = $created_time;
                    $photos[$i]["created_at"] = $created_time;
                }
                Photo::insert($photos);
            }
            if ($request->has('tags')) {
                $oldTags = TagSubscription::where('hadith_id', $id)->pluck('tag_id')->toArray();
                $newTags = $request->get('tags');
                if (!is_array($newTags)) {
                    $newTags = [];
                }
                if (!is_array($oldTags)) {
                    $oldTags = [];
                }
                if (is_array($newTags) && is_array($oldTags)) {
                    $dataSet = [];
                    $oldTags = array_map(function($e) {
                        return (int) $e;
                    }, $oldTags);
                    $newTags = array_map(function($e) {
                        return (int) $e;
                    }, $newTags);

                    $removeTags = array_diff($oldTags, $newTags);
                    $addTags = array_diff($newTags, $oldTags);
                    if (count($addTags) > 0) {
                        foreach ($addTags as $tag) {
                            $dataSet[] = [
                                'hadith_id' => $id,
                                'tag_id' => $tag,
                            ];
                        }
                        TagSubscription::insert($dataSet);
                    }
                    if (count($removeTags) > 0) {
                        TagSubscription::where('hadith_id', $id)->whereIn('tag_id', $removeTags)->delete();
                    }
                }
            }

            if ($request->has('category')) {
                $subscription = CategorySubscription::where('hadith_id', $id)->get();
                $subscription->category_id = $request->get('category');
                if ($subscription->save()) {
                    return redirect()->back()->with('success', 'success');
                }
            }
            return redirect()->back()->with('success', 'success adding hadith but without updating subscription');
        } else {
            return redirect()->back()->with('fail', "fail");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hadith  $hadith
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Hadith::destroy($id)) {
            CategorySubscription::where('hadith_id',$id)->delete();
            TagSubscription::where('hadith_id',$id)->delete();
            Photo::where('hadith_id',$id)->delete();
            return redirect()->back()->with('success', 'Delete Success');
        } else {
            return redirect()->back()->with('fail', "Delete Fail");
        }
    }

    public function getHadithsByCategoryId($id) {
        $subscription_ids = CategorySubscription::where('category_id', $id)->get()->pluck('hadith_id')->toArray();
        $hadiths = Hadith::whereIn('id', $subscription_ids)->get()->toArray();
        return response($hadiths, 200);
    }

    public function getHadithsByType($type) {
        $hadiths = Hadith::where('type', $type)->get();
        return response($hadiths, 200);
    }
    
    public function getHadithsByTagId($id) {
        $subscription_ids = TagSubscription::where('tag_id', $id)->get()->pluck('hadith_id')->toArray();
        $hadiths = Hadith::whereIn('id', $subscription_ids)->get()->toArray();
        $hadithsPhotos = Photo::whereIn('hadith_id', $subscription_ids)->get()->toArray();
        dd($hadithsPhotos);
        return response($hadiths, 200);
    }

}
