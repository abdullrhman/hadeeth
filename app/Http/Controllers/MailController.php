<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;

class MailController extends Controller {

    public function welcomeMail() {

        $data = ["data"];
        try {
            $mail = Mail::send('mail.mail', $data, function ($message) {
                $message->from('accounts@tebkhana.com', 'TEBKHANA');
                $message->subject("Welcome");
                $message->to('icecap16@gmail.com');
            });
        } catch (Exception $e) {
            
        }

        
        dd($mail);
    }

    public static function userRegistered($user) {
        $data = ["name" => $user->first_name . " " . $user->last_name , "email"=>$user->email];
        $admin = 'admin@tebkhana.com';
        Mail::send('mail.mail', $data, function ($message) use ($admin) {
            $message->from('accounts@tebkhana.com', 'TEBKHANA');
            $message->subject("Welcome");
            $message->to($admin);
        });
    }

    public static function youAreApproved($user) {
        $data = ["name" => $user->first_name . " " . $user->last_name];
        Mail::send('mail.welcome', $data, function ($message) use($user) {
            $message->from('accounts@tebkhana.com', 'TEBKHANA');
            $message->subject("Welcome");
            $message->to($user->email);
        });
    }

}
