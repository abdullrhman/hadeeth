<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Photo;


class PhotosController extends Controller {
    
    public function destroy($id) {
        if (Photo::destroy($id)) {
            return redirect()->back()->with('success', 'Delete Success');
        } else {
            return redirect()->back()->with('fail', "Delete Fail");
        }
    }

}
