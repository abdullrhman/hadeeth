@extends('layouts.admin')

@section('title')
Hadeeth - Edit User
@endsection
@section('extra-css')

@endsection
@section('content')
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Edit User</h2>
    </header>
    <!-- start: page -->
    <div class="row">
        <div class="col-md-12">

            <form class="form-horizontal" action="{{ url('/adminPanel/updateDoctor')}} "  method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$user->id}}"/>
                <section class="panel">
                    <header class="panel-heading">
                        @if(session()->has('success'))
                        <h4 class="alert alert-success">
                            {{ session()->get('success') }}
                        </h4>
                        @endif
                        @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                        @endif
                    </header>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Gender</label>
                            <div class="col-sm-9">
                                <select name="gender" id="gender" class="form-control" title="Please select your gender" required>     
                                    @if ($user->gender == 1)
                                        <option value="0">male</option>
                                        <option selected="selected" value="{{$user->gender}}">female</option>
                                    @else
                                        <option value="1">famale</option>
                                        <option selected="selected" value="{{$user->gender}}">male</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">First Name <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="first_name" class="form-control" value="{{$user->first_name}}"  placeholder="eg.: John Doe" required/>
                            </div>
                            @if ($errors->has('first_name'))                               
                            <strong class="col-sm-6 center text-danger">{{ $errors->first('first_name') }}</strong>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Last Name <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="last_name" class="form-control" value="{{$user->last_name}}"  placeholder="eg.: John Doe" required/>
                            </div>
                            @if ($errors->has('last_name'))                               
                            <strong class="col-sm-6 center text-danger">{{ $errors->first('last_name') }}</strong>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                    <input type="email" name="email" value="{{$user->email}}" class="form-control" placeholder="eg.: email@email.com" required/>
                                    @if ($errors->has('email'))                               
                                    <strong class="col-sm-6 center text-danger">{{ $errors->first('email') }}</strong>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-9">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Password </label>
                            <div class="col-sm-9">
                                <input type="password" name="password" class="form-control" placeholder="password" />
                            </div>
                            @if ($errors->has('password'))                               
                            <strong class="col-sm-6 center text-danger">{{ $errors->first('password') }}</strong>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> confirm password </label>
                            <div class="col-sm-9">
                                <input type="password" name="password_confirmation" class="form-control" placeholder="password" />
                            </div>
                            @if ($errors->has('password_confirmation'))                               
                            <strong class="col-sm-6 center text-danger">{{ $errors->first('password_confirmation') }}</strong>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Specialty</label>
                            <div class="col-sm-9">
                                <select name="speciality" id="speciality" class="form-control" title="Please select one specialty" required>
                                    @if(isset($specialities))
                                        @foreach ($specialities as $speciality)
                                            @if ($speciality->id == $user->speciality_id)
                                                <option value="{{$speciality->id}}" selected="selected">{{$speciality->name}}</option>
                                            @else
                                                <option value="{{$speciality->id}}">{{$speciality->name}}</option>
                                            @endif
                                        @endforeach
                                    @else
                                        <option  disabled="disabled" selected>there are no specialities yet</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Country</label>
                            <div class="col-sm-9">
                                <select onchange="getCities()" value="{{$user->country}}" id="country" name="country" class="form-control" title="Please select your country" required>
                                    @if (!$countries)
                                    <option  disabled="disabled" selected>there are no countries yet</option>
                                    @endif
                                    @if ($countries)
                                    @foreach ($countries as $country)
                                        @if ($country->id == $user->country)
                                            <option value="{{$country->id}}" selected="selected">{{$country->name}}</option>
                                        @else
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endif
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="city_holder">
                            <label class="col-sm-3 control-label">city</label>
                            <div class="col-sm-9">
                                <select  value="{{$user->city}}" id="city" name="city" class="form-control" title="Please select your city" required>
                                    @if (isset($cities))
                                        @foreach ($cities as $city)
                                            @if ($city->id == $user->city)
                                                <option value="{{$city->id}}" selected="selected">{{$city->name}}</option>
                                            @else
                                                <option value="{{$city->id}}">{{$city->name}}</option>
                                            @endif
                                        @endforeach
                                    @else
                                        <option  disabled="disabled" selected>there are no countries yet</option>
                                    @endif
                                </select>
                            </div>

                        </div> 
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mobile</label>
                            <div class="col-sm-9">
                                <input type="phone" value="{{$user->phone}}" name="phone" class="form-control" placeholder="mobile number" maxlength="11"/>
                            </div>
                            @if ($errors->has('phone'))                               
                            <strong class="col-sm-6 center text-danger">{{ $errors->first('phone') }}</strong>
                            @endif
                        </div> 
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Address</label>
                            <div class="col-sm-9">
                                <input type="text" value="{{$user->address}}" name="address" class="form-control" placeholder="Street no. Block no. ,City ,Area" />
                            </div>
                            @if ($errors->has('address'))                               
                            <strong class="col-sm-6 center text-danger">{{ $errors->first('address') }}</strong>
                            @endif
                        </div>                           
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </div>
                        </div>
                    </footer>
                </section>
            </form>
        </div> 
    </div>
    <!-- end: page -->
</section>

@endsection
@section('extra-js')
<!-- Specific Page Vendor -->
<script src="{{url('/custom/register_edit.js')}}"></script>
<script src="{{url('/admin_dashboard/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{url('/admin_dashboard/assets/vendor/select2/js/select2.js')}}"></script>
<!-- Examples -->
<script src="{{url('/admin_dashboard/assets/javascripts/forms/add.validation.js')}}"></script>

@endsection