@extends('layouts.app_admin')

@section('title')
Dashboard - Settings - Reset Password
@endsection
@section('extra-css')

@endsection

@section('content')
<div >
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Reset Password</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Reset Password
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            @if (Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li class="list-unstyled"> 
                                        {!! Session::get('success') !!}
                                    </li>
                                </ul>
                            </div>
                            @endif

                            @if (Session::has('fail'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li class="list-unstyled">
                                        {!! Session::get('fail') !!}
                                    </li>
                                </ul>
                            </div>
                            @endif
                            <form role="form" action="{{ route('reset_password') }}" method='POST'>
                                @csrf
                                <div class="form-group">
                                    <label>Old Password</label>
                                    <input type="password" class="form-control" name='password' placeholder="Enter your Old Password">
                                </div>
                                @if ($errors->has('password'))                               
                                <strong class="col-sm-6 center text-danger">{{ $errors->first('password') }}</strong>
                                @endif
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" class="form-control" name='new_password' placeholder="Enter your Old Password">
                                </div>
                                @if ($errors->has('new_password'))                               
                                <strong class="col-sm-6 center text-danger">{{ $errors->first('new_password') }}</strong>
                                @endif
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" name='confirm' placeholder="Enter your Old Password">
                                </div>
                                @if ($errors->has('confirm'))                               
                                <strong class="col-sm-6 center text-danger">{{ $errors->first('confirm') }}</strong>
                                @endif
                                <button type="submit" class="btn btn-default">Submit Button</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>                              
                            </form>
                        </div>
                        <!-- /.col-lg-12 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
@endsection

@section('extra-js')

@endsection

