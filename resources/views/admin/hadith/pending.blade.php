@extends('layouts.admin')

@section('title')
Dr Assistant - Home - Admin Panel
@endsection
@section('extra-css')
<link rel="stylesheet" href="{{url('/dashboard/plugins/datatables/datatable.min.css')}}">

@endsection
@section('content')
<section  role="main" class="content-body">
    <div id="tableSection">
        <header class="page-header">
            <h2>All The Users</h2>
        </header>
        <!-- start: page -->
        <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <header class="panel-heading">
                        @if(session()->has('success'))
                        <h6 class="panel-title text-success">
                            {{ session()->get('success') }}
                        </h6>
                        @endif
                        @if(session()->has('error'))
                        <h6 class="panel-title text-danger">
                            {{ session()->get('error') }}
                        </h6>
                        @endif
                    </header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="table" class="table table-hover mb-hide">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>email</th>
                                        <th>status</th>
                                        <th>Active</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if(isset($users))
                                    @foreach($users as $user) 
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->first_name." ".$user->last_name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>
                                            @if($user->status == 1)
                                            <span class="text-success">Active</span>
                                            @else
                                            <span class="text-danger">Deactive</span>
                                            @endif
                                        </td>
                                        <td class=" actions-fade">
                                            <a  onclick="viewUser({{$user->id}})"  href="javascript:void(0)"><i class="fa fa-male btn btn-primary"></i></a>
                                            <a  onclick="confirmActive({{$user->id}})"  href="javascript:void(0)"><i class="fa fa-check btn btn-primary"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="5"> There are no users yet</td>
                                    </tr>
                                    @endif


                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- end: page -->
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div id="dialog" class="modal-block mfp-hide">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Are you sure?</h2>
                    </header>
                    <div class="panel-body">
                        <div class="modal-wrapper">
                            <div class="modal-text">
                                <p>Are you sure that you want to delete this User ?</p>
                            </div>
                        </div>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button onclick="dialogConfirm()" id="dialogConfirm" class="btn btn-primary">Confirm</button>
                                <button onclick="dialogCancel()" id="dialogCancel" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </footer>
                </section>
            </div>
            <div id="userDialog" class="modal-block mfp-hide">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title" id="userFullName">User</h2>
                        <h4 class="panel-title" id="userEmail">Email</h4>
                    </header>
                    <div class="panel-body">
                        <div class="modal-wrapper">
                            <div class="modal-text">
                                <img id="userImage" src="" style="height:100%; width:100%;"/>
                            </div>
                        </div>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button onclick="dialogConfirm()" id="dialogConfirm" class="btn btn-primary">Confirm</button>
                                <button onclick="dialogCancel()" id="dialogCancel" class="btn btn-default">Cancel</button>
                            </div>
                        </div>
                    </footer>
                </section>
            </div>
        </div>
    </div>
</section>
@endsection
@section('extra-js')
<script>
    $(document).ready(function() {
    $('#table').DataTable();
    });
    var table = document.getElementById('tableSection');
    var dialog = document.getElementById('dialog');
    var sideBar = document.querySelector("html");
    var userid = "";
    var _self = this;
    function confirmActive(id){
    _self.userid = id;
    table.classList.add('mfp-hide');
    table.classList.remove('mfp-none');
    dialog.classList.add('mfp-none');
    dialog.classList.remove('mfp-hide');
    sideBar.classList.add('sidebar-left-collapsed');
    }
    //sideBar.classList.remove('sidebar-left-collapsed');
    function viewUser(id){
    _self.userid = id;
    table.classList.add('mfp-hide');
    table.classList.remove('mfp-none');
    userDialog.classList.add('mfp-none');
    userDialog.classList.remove('mfp-hide');
    sideBar.classList.add('sidebar-left-collapsed');
    userURL = "/adminPanel/getUser/" + userid;
    $.get(userURL, function(user){
    this.user = user;
    userImage = document.querySelector("#userImage");
    userFullName = document.querySelector("#userFullName");
    userImage.src = this.user[0].license?"/" + this.user[0].license : "/dashboard/images/image_placeholder.jpg";
    userFullName.innerHTML = this.user[0].first_name + " " + this.user[0].last_name;
    userEmail.innerHTML = this.user[0].email;
    console.log(this.user[0]);
    });
    }


    function dialogConfirm(){
    window.location = "/adminPanel/activeUser/" + _self.userid;
    }
    function dialogCancel(){
    _self.userid = "";
    userDialog.classList.add('mfp-hide');
    userDialog.classList.remove('mfp-none');
    dialog.classList.add('mfp-hide');
    dialog.classList.remove('mfp-none');
    table.classList.add('mfp-none');
    table.classList.remove('mfp-hide');
    }

</script>
@endsection