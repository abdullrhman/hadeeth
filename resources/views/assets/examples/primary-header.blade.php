<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light container">
        <a class="navbar-brand mobile" href="#">Dr.Assistant</a>
        <button class="navbar-toggler" type="button">
            <span class="fa fa-bars"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('/') ? 'active' :'' }}" href="{{url('/')}}"> <i class="fa fa-home fa-lg"></i> &nbsp; {!! trans('nav.home') !!}</a>
                </li>
            </ul>
            @guest
            <ul class="navbar-nav">
                
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('register') ? 'active' :'' }}" href="{{route('register')}}"><i class="fa fa-user-plus fa-lg"></i> &nbsp; {!! trans('nav.register') !!}</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('login') ? 'active' :'' }}" href="{{route('login')}}"><i class="fa fa-sign-in fa-lg"> </i> &nbsp; {!! trans('nav.login') !!}</a>
                </li>
            </ul>
            @else
            
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/home')}}"><i class="fa fa-dashboard"></i> &nbsp; {!! trans('nav.dashboard') !!}</a>
                    </li>
                    @if( Auth::User()->role == 0 )
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/adminPanel')}}"><i class="fa fa-dashboard"></i> &nbsp; {!! trans('Admin Dashboard') !!}</a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/logoff')}}"><i class="fa fa-sign-in fa-lg"> </i> &nbsp; {!! trans(' logout') !!}</a>
                    </li>
                </ul>
            @endguest
            
        </div>
        
    </nav>
</div>
