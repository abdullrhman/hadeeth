<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="{{url('/dashboard/vendor/bootstrap/css/bootstrap.min.css')}}">
<!-- MetisMenu CSS -->
<link rel="stylesheet" href="{{url('/dashboard/vendor/metisMenu/metisMenu.min.css')}}">
<!-- Custom CSS -->
<link rel="stylesheet" href="{{url('/dashboard/dist/css/sb-admin-2.css')}}">
<!-- Morris Charts CSS -->
<link rel="stylesheet" href="{{url('/dashboard/vendor/morrisjs/morris.css')}}">
<!-- Custom Fonts -->
<link rel="stylesheet" href="{{url('/dashboard/vendor/font-awesome/css/font-awesome.min.css')}}">
